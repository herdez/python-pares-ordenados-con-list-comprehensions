## Nested list comprehension con estructura condicional 'if'

Un ejemplo de nested list comprehensions utilizando condicional `if` es el siguiente:

```python
"""Usando for loop"""
def list_comprehension_function(x, y, n):
	    values = []
	    for i in range(x + 1):
	    	for j in range(y + 1):
	            if ( i + j ) != n:
	                values.append([i, j])
	    return values

print(list_comprehension_function(1, 1, 2)) == [[0, 0], [0, 1], [1, 0]])

```

Utilizando list comprehensions:

```python
"""Usando nested list comprehensions con condicional if"""
def list_comprehension_function(x, y, n):
	return [ [i, j] for i in range(x + 1) for j in range(y + 1) if ( ( i + j ) != n )]

print(list_comprehension_function(1, 1, 2) == [[0, 0], [0, 1], [1, 0]])

```

## Ejercicio - Pares ordenados con list comprehensions


Define la función `list_in_order` con los parámetros `x`, `y`, `z` y `n`. Utiliza list comprehensions para formar los pares ordenados `[i, j, k]`. Los argumentos para cada parámetro y la lista con los pares ordenados que deberá formarse se muestra en el siguiente `driver code`:

```python
#list_in_order function


#driver code
print(list_in_order(1, 1, 1, 2) == [[0, 0, 0], [0, 0, 1], [0, 1, 0], [1, 0, 0], [1, 1, 1]])
print(list_in_order(2, 2, 2, 2) == [[0, 0, 0], [0, 0, 1], [0, 1, 0], [0, 1, 2], [0, 2, 1], [0, 2, 2], [1, 0, 0], [1, 0, 2], [1, 1, 1], [1, 1, 2], [1, 2, 0], [1, 2, 1], [1, 2, 2], [2, 0, 1], [2, 0, 2], [2, 1, 0], [2, 1, 1], [2, 1, 2], [2, 2, 0], [2, 2, 1], [2, 2, 2]])
``` 

Para este ejercicio son dados los valores `x`, `y`, `z` y `n`:

```python
#ejemplo de valores para x, y, z y n.

x = 1
y = 1
z = 1
n = 2
```

Es necesario encontrar los pares ordenados (i, j, k) donde la suma de `i + j + k` sea diferente de `n`. El valor de i es de `0 a x`, el valor de j es de `0 a y`, el valor de k es de `0 a z`.

Un ejemplo de la secuencia de salida para los valores dados es el siguiente:

```python
#Inicio de iteración para i que va de 0 a 1
for loop i = 0
[]
#Inicio de iteración para j que va de 0 a 1
for loop j = 0
[]
#Inicio de iteración para k que va de 0 a 1
for loop k = 0
[[0,0,0]] 										   #i + j + k = 0
#Segunda iteración para k
for loop k = 1     
[[0,0,0], [0,0,1]]								   #i + j + k = 1
#Segunda iteración para j
for loop j = 1
[[0,0,0], [0,0,1]]
#Primera iteración para k que va de 0 a 1
for loop k = 0     
[[0,0,0], [0,0,1], [0,1,0]] 						 #i + j + k = 1
#Segunda iteración para k
for loop k = 1     
[[0,0,0], [0,0,1], [0,1,0]] #Misma lista			#i + j + k = 2
#Segunda iteración para i
for loop i = 1
[[0,0,0], [0,0,1], [0,1,0]]
#Primera iteración para j que va de 0 a 1
for loop j = 0
[[0,0,0], [0,0,1], [0,1,0]]
#Primera iteración para k que va de 0 a 1
for loop k = 0      
[[0,0,0], [0,0,1], [0,1,0], [1,0,0]] 				#i + j + k = 1
#Segunda iteración para k
for loop k = 1      					
[[0,0,0], [0,0,1], [0,1,0], [1,0,0]] #Misma lista	#i + j + k = 2
#Segunda iteración para j
for loop j = 1
[[0,0,0], [0,0,1], [0,1,0], [1,0,0]]
#Primera iteración para k que va de 0 a 1
for loop k = 0      
[[0,0,0], [0,0,1], [0,1,0], [1,0,0]] #Misma lista	#i + j + k = 2
#Segunda iteración para k
for loop k = 1      
[[0,0,0], [0,0,1], [0,1,0], [1,0,0], [1,1,1]] 	   #i + j + k = 3

```
