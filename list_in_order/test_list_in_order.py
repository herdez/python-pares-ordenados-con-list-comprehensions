import unittest
from list_in_order import *

class ListInOrderTests(unittest.TestCase):

	def test_list1_four_numbers(self):
		self.assertEqual(list_in_order(1, 1, 1, 2), [[0, 0, 0], [0, 0, 1], [0, 1, 0], [1, 0, 0], [1, 1, 1]])

	def test_list2_four_numbers(self):
		self.assertEqual(list_in_order(2, 2, 2, 2), [[0, 0, 0], [0, 0, 1], [0, 1, 0], [0, 1, 2], [0, 2, 1], [0, 2, 2], [1, 0, 0], [1, 0, 2], [1, 1, 1], [1, 1, 2], [1, 2, 0], [1, 2, 1], [1, 2, 2], [2, 0, 1], [2, 0, 2], [2, 1, 0], [2, 1, 1], [2, 1, 2], [2, 2, 0], [2, 2, 1], [2, 2, 2]])
		
if __name__=="__main__":
    unittest.main()